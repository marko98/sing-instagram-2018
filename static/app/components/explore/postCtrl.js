(function(angular){
    var app = angular.module("MyApp");

    app.controller("PostCtrl", ["$http", "$state", "$interval", "$rootScope", "$window", function ($http, $state, $interval, $rootScope, $window) {
        var vm = this;
        
        vm.post = {};
        vm.comments = [];
        vm.lastComments = [];
        vm.foundPersonName;
        vm.params = {
            "userName": $state.params.username,
            "userId": $state.params.userId,
            "otherUserId": $state.params.otherUserId,
            "postId": $state.params.postId,
        };
        vm.newComment = {
            "postId": $state.params.postId,
            "userId": $state.params.userId,
            "content": ""
        };
        vm.isLiked = false;

        vm.showLocation = function(latitude, longitude){
            $window.open("https://www.latlong.net/c/?lat=" + latitude + "&long=" + longitude);
        };

        vm.startInterval = function(){
            $rootScope.intervalPost = $interval(function(){
                // console.log("radi post");
                vm.getComments();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalPost);
        };

        vm.getFriend = function(){
            params = {
                "foundPersonId": $state.params.otherUserId
            };
            $http.get("/users", {params: params}).then(function(response){
                vm.foundPersonName = response.data.username;
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.isMine = function(id){
            if(id == $state.params.userId){
                return true;
            } else {
                return false;
            }
        };

        vm.loadMoreComments = function(){
            if(vm.comments.length > vm.lastComments.length){
                var count = vm.comments.length - vm.lastComments.length;
                if(count >= 10){
                    var data = [];
                    var to = vm.comments.length - 10;
                    for(let i = to-10; i < to; i++){
                        data.push(vm.comments[i]);
                    };
                    for(let j = 0; j < vm.lastComments.length; j++){
                        data.push(vm.lastComments[j]);
                    };
                    vm.lastComments = data;
                } else {
                    var data = [];
                    var to = count;
                    for(let i = 0; i < to; i++){
                        data.push(vm.comments[i]);
                    };
                    for(let j = 0; j < vm.lastComments.length; j++){
                        data.push(vm.lastComments[j]);
                    };
                    vm.lastComments = data;
                };
            };
        };

        vm.fillLastComments = function(numberOfComments){
            var j = vm.comments.length - numberOfComments;
            for(j; j < vm.comments.length; j++){
                vm.lastComments.push(vm.comments[j]);
            };
        };

        vm.showDate = function(id){
            for(let i = 0; i < vm.lastComments.length; i++){
                if(vm.lastComments[i].comment.comment_id == id){
                    if(vm.lastComments[i].showDate == false){
                        vm.lastComments[i].showDate = true;
                    } else {
                        vm.lastComments[i].showDate = false;
                    };
                };
            };
        };

        vm.getComments = function(){
            $http.get("/comments", {params: vm.params}).then(function(response){
                if(response.data.length > 0){
                    vm.comments = [];
                    for(let i = response.data.length-1; i >= 0; i--){
                        vm.comments.push(response.data[i]);
                    };
                };
                if(vm.comments.length > 10){
                    vm.lastComments = [];
                    vm.fillLastComments(10);
                } else {
                    vm.lastComments = vm.comments;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.sendNewComment = function(){
            if(vm.newComment.content == ""){
                return;
            };
            $http.post("/comments", vm.newComment).then(function(response) {
                vm.newComment.content = "";
                vm.comments = [];
                vm.lastComments = [];
                vm.getComments();
            }, function() {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.goExplore = function(){
            vm.stop();
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goSearch = function(){
            vm.stop();
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goEvents = function(){
            vm.stop();
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            vm.stop();
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goToProfile = function(){
            vm.stop();
            $state.go('foundPerson', {username: $state.params.username, userId: $state.params.userId, foundPersonId: $state.params.otherUserId});
        };

        vm.changePic = function (direction){
            if(direction == 'toLeft'){
                if(vm.post.show - 1 < 0){
                    vm.post.show = vm.post.urls.length - 1;
                } else {
                    vm.post.show = vm.post.show - 1;
                };
            } else {
                if(vm.post.show + 1 == vm.post.urls.length){
                    vm.post.show = 0;
                } else {
                    vm.post.show = vm.post.show + 1;
                };
            }; 
        };

        vm.likePost = function(postId){
            data = {
                "postId": postId,
                "userId": $state.params.userId
            };
            $http.post("/likes", data).then(function(response) {
                vm.isLiked = true;
            }, function() {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.dislikePost = function(postId){
            data = {
                "postId": postId,
                "userId": $state.params.userId
            };
            $http.delete("/likes", {params: data}).then(function(response){
                vm.isLiked = false;
            }, function(response){
                console.log("Error! Code: " + response.status);
            });
        };

        vm.isLiked = function(){
            data = {
                "postId": $state.params.postId,
                "userId": $state.params.userId
            };
            $http.get("/likes", {params: data}).then(function(response){
                vm.isLiked = response.data;
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.getPost = function(){
            data = {
                "postId": $state.params.postId
            };
            $http.get("/posts", {params: data}).then(function(response){
                var paths = response.data[0].post_url.split(";");
                var path = paths.shift();
                var username = path.split("/");
                username = username[1];
                var date = response.data[0].date_updated.split("T");
                date = date[0] + " " + date[1];
                var post = {
                    "path": path,
                    "onePage": true,
                    "show": 0,
                    "username": username,
                    "urls": paths,
                    "data": response.data[0],
                    "date": date
                };

                if(paths.length > 1){
                    post.onePage = false;
                };
                vm.post = post;
                vm.getFriend();
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getPost();
            vm.getComments();
            vm.isLiked();
        };

        vm.onLoad();

    }]);

})(angular);