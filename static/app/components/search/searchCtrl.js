(function(angular){
    var app = angular.module("MyApp");

    app.controller("SearchCtrl", ["$state", "$http", "$interval", "$rootScope", function ($state, $http, $interval, $rootScope) {
        var vm = this;
        
        vm.currentUserName = $state.params.username;
        vm.params = {
            "usernameSearch": "",
            "userId": $state.params.userId
        };
        vm.person;
        vm.found = [];
        vm.uploads = [];

        vm.startInterval = function(){
            $rootScope.intervalSearch = $interval(function(){
                // console.log("radi search");
                vm.getUploads();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalSearch);
        };

        vm.find = function(userId){
            vm.stop();
            $state.go('foundPerson', {username: $state.params.username, userId: $state.params.userId, foundPersonId: userId});
        };

        vm.changePic = function(index, direction){
            if(direction == 'toLeft'){
                if(vm.uploads[index].show - 1 < 0){
                    vm.uploads[index].show = vm.uploads[index].urls.length - 1;
                } else {
                    vm.uploads[index].show = vm.uploads[index].show - 1;
                };
            } else {
                if(vm.uploads[index].show + 1 == vm.uploads[index].urls.length){
                    vm.uploads[index].show = 0;
                } else {
                    vm.uploads[index].show = vm.uploads[index].show + 1;
                };
            }; 
        };

        vm.seePost = function(postId, userId){
            vm.stop();
            $state.go('post', {username: $state.params.username, userId: $state.params.userId, otherUserId: userId, postId: postId});
        };

        vm.getUploads = function(){
            params = vm.params;
            params["action"] = "search";
            $http.get("/posts", {params: params}).then(function(response){
                if(response.data.length > 0){
                    vm.uploads = [];
                    for(let i = 0; i < response.data.length; i++){
                        var paths = response.data[i].post_url.split(";");
                        var path = paths.shift();
                        var username = path.split("/");
                        username = username[1];
                        var post = {
                            "username": username,
                            "path": path,
                            "onePage": true,
                            "show": 0,
                            "urls": paths,
                            "data": response.data[i]
                        };
                        if(paths.length > 1){
                            post.onePage = false;
                        };
                        vm.uploads.push(post);
                    };
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.goExplore = function(){
            vm.stop();
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.search = function(){
            $http.get("/users", {params: vm.params}).then(function(response){
                vm.found = response.data;
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.goEvents = function(){
            vm.stop();
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            vm.stop();
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goToProfile = function(otherUserId){
            vm.stop();
            $state.go('foundPerson', {username: $state.params.username, userId: $state.params.userId, foundPersonId: otherUserId});
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getUploads();
        };

        vm.onLoad();

    }]);

})(angular);