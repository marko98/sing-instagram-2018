(function(angular){
    var app = angular.module("MyApp", ["ui.router", "ui.bootstrap", "ngFileUpload"]);

    app.config(["$stateProvider", "$urlRouterProvider", "$transitionsProvider", function($stateProvider, $urlRouterProvider, $transitionsProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "sign_up",
            url: "/www.instagram.com/signup",
            templateUrl: "app/components/login_or_sign_up/sign_up.tpl.html",
            controller: "LoginOrSignUpCtrl",
            controllerAs: "losuc"
        }).state({
            name: "login",
            url: "/www.instagram.com/login",
            templateUrl: "app/components/login_or_sign_up/login.tpl.html",
            controller: "LoginOrSignUpCtrl",
            controllerAs: "losuc"
        }).state({
            name: "upload",
            url: "/www.instagram.com/user/:username/:userId/upload",
            templateUrl: "app/components/upload/upload.tpl.html",
            controller: "UploadCtrl",
            controllerAs: "uc",
            params: {
                username: null,
                userId: null,
                user: null,
                upload: null
            }
        }).state({
            name: "user",
            url: "/www.instagram.com/user/:username/:userId",
            templateUrl: "app/components/user/user.tpl.html",
            controller: "UserCtrl",
            controllerAs: "uc",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "editProfile",
            url: "/www.instagram.com/user/:username/:userId/edit",
            templateUrl: "app/components/user/editProfile.tpl.html",
            controller: "EditProfileCtrl",
            controllerAs: "epc",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "followers",
            url: "/www.instagram.com/user/:username/:userId/followers",
            templateUrl: "app/components/user/followers.tpl.html",
            controller: "FollowersCtrl",
            controllerAs: "fc",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "followings",
            url: "/www.instagram.com/user/:username/:userId/followings",
            templateUrl: "app/components/user/followings.tpl.html",
            controller: "FollowingsCtrl",
            controllerAs: "fc",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "events",
            url: "/www.instagram.com/user/:username/:userId/events",
            templateUrl: "app/components/events/events.tpl.html",
            controller: "EventsCtrl",
            controllerAs: "ec",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "search",
            url: "/www.instagram.com/user/:username/:userId/search",
            templateUrl: "app/components/search/search.tpl.html",
            controller: "SearchCtrl",
            controllerAs: "sc",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "foundPerson",
            url: "/www.instagram.com/user/:username/:userId/search/:foundPersonId",
            templateUrl: "app/components/search/foundPerson.tpl.html",
            controller: "FoundPersonCtrl",
            controllerAs: "fpc",
            params: {
                username: null,
                userId: null,
                foundPersonId: null
            }
        }).state({
            name: "foundPersonFollowers",
            url: "/www.instagram.com/user/:username/:userId/search/:foundPersonUsername/:foundPersonId/followers",
            templateUrl: "app/components/search/foundPersonFollowers.tpl.html",
            controller: "FoundPersonFollowersCtrl",
            controllerAs: "fpfc",
            params: {
                username: null,
                userId: null,
                foundPersonUsername: null,
                foundPersonId: null
            }
        }).state({
            name: "foundPersonFollowings",
            url: "/www.instagram.com/user/:username/:userId/search/:foundPersonUsername/:foundPersonId/followings",
            templateUrl: "app/components/search/foundPersonFollowings.tpl.html",
            controller: "FoundPersonFollowingsCtrl",
            controllerAs: "fpfc",
            params: {
                username: null,
                userId: null,
                foundPersonUsername: null,
                foundPersonId: null
            }
        }).state({
            name: "explore",
            url: "/www.instagram.com/user/:username/:userId/explore",
            templateUrl: "app/components/explore/explore.tpl.html",
            controller: "ExploreCtrl",
            controllerAs: "ec",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "post",
            url: "/www.instagram.com/user/:username/:userId/explore/:otherUserId/:postId",
            templateUrl: "app/components/explore/post.tpl.html",
            controller: "PostCtrl",
            controllerAs: "pc",
            params: {
                username: null,
                userId: null,
                otherUserId: null,
                postId: null
            }
        }).state({
            name: "messages",
            url: "/www.instagram.com/user/:username/:userId/messages",
            templateUrl: "app/components/explore/messages.tpl.html",
            controller: "MessagesCtrl",
            controllerAs: "mc",
            params: {
                username: null,
                userId: null
            }
        }).state({
            name: "message",
            url: "/www.instagram.com/user/:username/:userId/messages/:friendId",
            templateUrl: "app/components/explore/message.tpl.html",
            controller: "MessageCtrl",
            controllerAs: "mc",
            params: {
                username: null,
                userId: null,
                friendId: null
            }
        });

        $urlRouterProvider.otherwise("/www.instagram.com/signup");
    }]);
})(angular)