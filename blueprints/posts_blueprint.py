import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

posts_blueprint = Blueprint("posts_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

# GET ALL POSTS
@posts_blueprint.route("", methods=["GET"])
@secured()
def get_all_posts():
    # print(request.args)
    query = "SELECT * FROM posts"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()

    # SEARCH ------------------------------------------------------------------------------------
    if request.args.get("action") is not None and request.args.get("action") == "search":
        print("usao")
        # "SELECT * FROM instagram.posts WHERE user_id != 1 ORDER BY date_updated DESC;"
        if request.args.get("userId") is not None and request.args.get("userId") is not "":
            params.append(request.args.get("userId"))
            selection += "user_id != %s "
        
        if len(params) > 0:
            query += selection
        
        query += "ORDER BY date_updated DESC"
        cr.execute(query, params)
        posts = cr.fetchall()
        return flask.json.jsonify(posts)
    # ---------------------------------------------------------------------------------------------

    # EXPLORE ------------------------------------------------------------------------------------
    if request.args.get("action") is not None and request.args.get("action") == "explore":
        # "SELECT * FROM posts WHERE user_id LIKE 1 OR user_id LIKE 2 ORDER BY date_updated DESC"
        query2 = "SELECT * FROM posts WHERE "
        followingUsersId = request.args.get("followingUsersId")
        followingUsersId = followingUsersId.split(";")
        data = []
        data.append(request.args.get("userId"))
        for i in range(0, len(followingUsersId)):
            if followingUsersId[i] != "":
                data.append(followingUsersId[i])

        # print(data)
        for j in range(0, len(data)):
            query2 = query2 + "user_id LIKE " + data[j] + " "

            if len(data) > 1 and j is not len(data)-1:
                query2 = query2 + "OR "
        query2 = query2 + "ORDER BY date_updated DESC"
        # print(query2)
        cr.execute(query2,)
        posts = cr.fetchall()
        return flask.json.jsonify(posts)
    # --------------------------------------------------------------------------------------------

    # FOUND PERSON ------------------------------------------------------------------------------------
    if request.args.get("foundPersonId") is not None and request.args.get("foundPersonId") is not "":
        params.append(request.args.get("foundPersonId"))
        selection += "user_id LIKE %s "
    # ------------------------------------------------------------------------------------------------

    #  USER PAGE -------------------------------------------------------------------------
    elif request.args.get("userId") is not None and request.args.get("userId") is not "":
        params.append(request.args.get("userId"))
        selection += "user_id LIKE %s "
    # ------------------------------------------------------------------------------------- 

    #  POST PAGE -------------------------------------------------------------------------
    elif request.args.get("postId") is not None and request.args.get("postId") is not "":
        params.append(request.args.get("postId"))
        selection += "post_id LIKE %s "
    # ------------------------------------------------------------------------------------- 
    
    if len(params) > 0:
        query += selection

    # print(query)
    # print(params)
    cr.execute(query, params)
    posts = cr.fetchall()
    for post in posts:
        if post != None:
            post["date_created"] = post["date_created"].isoformat()
            # post["date_created"] = post["date_created"].split("T")
            # post["date_created"] = post["date_created"][0]

            post["date_updated"] = post["date_updated"].isoformat()
            # post["date_updated"] = post["date_updated"].split("T")
            # post["date_updated"] = post["date_updated"][0]
    return flask.json.jsonify(posts)


@posts_blueprint.route("/<int:post_id>", methods=["DELETE"])
def delete_post(post_id):
    db = main.mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM comments WHERE post_id=%s", (post_id,))
    cr.execute("DELETE FROM likes WHERE post_id=%s", (post_id,))
    cr.execute("DELETE FROM posts WHERE post_id=%s", (post_id,))
    db.commit()
    return "", 204