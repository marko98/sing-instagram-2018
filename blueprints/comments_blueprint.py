import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

comments_blueprint = Blueprint("comments_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles


@comments_blueprint.route("", methods=["GET"])
@secured()
def get_comments():
    # "SELECT * FROM instagram.comments WHERE post_id LIKE 3 ORDER BY date_updated DESC;"
    query = "SELECT * FROM comments"
    selection = " WHERE "
    params = []
    data = []
    cr = main.mysql_db.get_db().cursor()
    # print(request.args)

    if request.args.get("postId") is not None and request.args.get("postId") is not "":
        params.append(int(request.args.get("postId")))
        selection += "post_id LIKE %s "

    if len(params) > 0:
        query += selection

    query += "ORDER BY date_updated DESC"
    # print(query)

    cr.execute(query, params)
    comments = cr.fetchall()
    for comment in comments:
        if comment != None:
            query2 = "SELECT * FROM users WHERE user_id LIKE %s "
            cr.execute(query2, (comment["user_id"], ))
            user = cr.fetchone()
            # comment["date_created"] = comment["date_created"].isoformat()
            # comment["date_updated"] = comment["date_updated"].isoformat()
            object_data = {
                "from": user,
                "comment": comment,
                "showDate": False
            }
            data.append(object_data)

    return flask.json.jsonify(data)
    

@comments_blueprint.route("", methods=["POST"])
@secured()
def add_comment():
    data = dict(request.json)
    # print(data)
    db = main.mysql_db.get_db()
    cr = db.cursor()
    data["dateCreated"] = datetime.datetime.now()
    data["dateUpdated"] = datetime.datetime.now()
    cr.execute("INSERT INTO comments (post_id, user_id, content, date_created, date_updated) VALUES(%(postId)s, %(userId)s, %(content)s, %(dateCreated)s, %(dateUpdated)s)", data)
    db.commit()
    return "", 201