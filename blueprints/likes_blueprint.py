import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

likes_blueprint = Blueprint("likes_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

@likes_blueprint.route("", methods=["GET"])
@secured()
def is_liked():
    query = "SELECT * FROM likes"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()
    data = []
    # print(request.args)


    if request.args.get("action") == "allLikes":
        print("uuuu")
        if request.args.get("postId") is not None and request.args.get("postId"):
            params.append(int(request.args.get("postId")))
            selection += "post_id LIKE %s "

        if len(params) > 0:
            query += selection

        cr.execute(query, params)
        likes = cr.fetchall()

        print("ssss")
        return flask.json.jsonify(likes)  

    if request.args.get("userId") is not None and request.args.get("userId") is not "":
        params.append(int(request.args.get("userId")))
        selection += "user_id LIKE %s "

    if request.args.get("postId") is not None and request.args.get("postId"):
        params.append(int(request.args.get("postId")))
        if len(params) > 1:
            selection += "AND "
        selection += "post_id LIKE %s "

    if len(params) > 0:
        query += selection

    cr.execute(query, params)
    like = cr.fetchone()
    if like != None:
        return flask.json.jsonify(True)
    return flask.json.jsonify(False)   

@likes_blueprint.route("", methods=["POST"])
@secured()
def add_like():
    data = dict(request.json)
    # print(data)
    db = main.mysql_db.get_db()
    cr = db.cursor()
    data["dateCreated"] = datetime.datetime.now()
    cr.execute("INSERT INTO likes (user_id, post_id, date_created) VALUES(%(userId)s, %(postId)s, %(dateCreated)s)", data)
    db.commit()
    return "", 201

@likes_blueprint.route("", methods=["DELETE"])
@secured()
def dislike():
    # print(request.args)
    user_id = request.args.get("userId")
    post_id = request.args.get("postId")
    db = main.mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM likes WHERE user_id=%s AND post_id=%s", (user_id, post_id, ))
    db.commit()
    return "", 204