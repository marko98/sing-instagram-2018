-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: instagram
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cities` (
  `city_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `state_id` int(20) NOT NULL,
  PRIMARY KEY (`city_id`),
  KEY `fk_cities_state_id` (`state_id`),
  CONSTRAINT `fk_cities_state_id` FOREIGN KEY (`state_id`) REFERENCES `states` (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'New York City',1),(2,'Brooklyn',1),(3,'Los Angeles',2),(4,'Montgomery',3),(5,'Birmingham',3),(6,'Juneau',5),(7,'Anchorage',5),(8,'Phoenix',4),(9,'Little Rock',6),(10,'Denver',7),(11,'Sacramento',2),(13,'Hartford',8),(14,'Bridgeport',8),(15,'Dover',9),(16,'Wilmington',9),(17,'Tallahassee',10),(18,'Jacksonville',10),(19,'Atlanta',11),(20,'Honolulu',12),(21,'Boise',13),(22,'Springfield',14),(23,'Chicago',14),(24,'Indianapolis',15),(25,'Des Moines',16),(26,'Topeka',17),(27,'Wichita',17),(28,'Frankfort',18),(29,'Louisville',18),(30,'Baton Rouge',19),(31,'New Orleans 	',19),(32,'Augusta',20),(33,'Portland',20),(34,'Annapolis',21),(35,'Baltimore',21),(36,'Boston',22),(37,'Lansing',23),(38,'Detroit',23),(39,'St. Paul',24),(40,'Minneapolis',24),(41,'Jackson',25),(42,'Jefferson City',26),(43,'Kansas City',26),(44,'Helena',27),(45,'Billings',27),(46,'Lincoln',28),(47,'Omaha',28),(48,'Carson City',29),(49,'Las Vegas',29),(50,'Concord',30),(51,'Manchester',30),(52,'Trenton',31),(53,'Newark',31),(54,'Santa Fe',32),(55,'Albuquerque',32),(56,'Albany',1),(57,'Raleigh',33),(58,'Charlotte',33),(59,'Bismarck',34),(60,'Fargo',34),(61,'Columbus',35),(62,'Oklahoma City',36),(63,'Salem',37),(64,'Portland',37),(65,'Harrisburg',38),(66,'Philadelphia',38),(67,'Providence',39),(68,'Columbia',40),(69,'Charleston',40),(70,'Pierre',41),(71,'Sioux Falls',41),(72,'Nashville',42),(73,'Austin',43),(74,'Houston',43),(75,'Salt Lake City',44),(76,'Montpelier',45),(77,'Burlington',45),(78,'Richmond',46),(79,'Virginia Beach',46),(80,'Olympia',47),(81,'Seattle',47),(82,'Charleston',48),(83,'Madison',49),(84,'Milwaukee',49),(85,'Cheyenne',50);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comments` (
  `comment_id` int(20) NOT NULL AUTO_INCREMENT,
  `post_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `fk_comments_user_id_idx` (`user_id`),
  KEY `fk_comments_post_id_idx` (`post_id`),
  CONSTRAINT `fk_comments_post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `fk_comments_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,21,5,'Great! :)','2019-01-10 19:06:24','2019-01-10 19:06:24'),(2,32,5,'hmmmm','2019-01-10 19:06:33','2019-01-10 19:06:33'),(3,31,5,'awesome','2019-01-10 19:06:41','2019-01-10 19:06:41'),(4,37,6,'nice','2019-01-10 19:08:24','2019-01-10 19:08:24'),(5,33,6,'yum','2019-01-10 19:09:29','2019-01-10 19:09:29'),(6,25,6,'magnificent','2019-01-10 19:10:18','2019-01-10 19:10:18'),(7,32,10,'yeah','2019-01-10 19:11:18','2019-01-10 19:11:18'),(8,35,10,'glorious','2019-01-10 19:11:37','2019-01-10 19:11:37'),(9,7,10,'so cute','2019-01-10 19:12:13','2019-01-10 19:12:13'),(10,3,10,'awesome','2019-01-10 19:12:37','2019-01-10 19:12:37'),(11,38,10,'like it :)','2019-01-10 19:12:47','2019-01-10 19:12:47'),(12,24,10,'beautiful','2019-01-10 19:13:16','2019-01-10 19:13:16'),(13,37,4,'cool','2019-01-10 19:15:01','2019-01-10 19:15:01'),(14,6,4,'love it','2019-01-10 19:15:12','2019-01-10 19:15:12'),(15,37,8,'yeah','2019-01-10 19:15:48','2019-01-10 19:15:48'),(16,37,8,'perfect','2019-01-10 19:15:54','2019-01-10 19:15:54'),(17,31,8,'yum','2019-01-10 19:16:08','2019-01-10 19:16:08'),(18,30,8,'cool','2019-01-10 19:16:15','2019-01-10 19:16:15'),(19,19,8,'frodo!','2019-01-10 19:16:28','2019-01-10 19:16:28'),(20,12,4,'interesting','2019-01-10 19:18:19','2019-01-10 19:18:19'),(21,13,4,'Fly me to the moon.','2019-01-10 19:18:28','2019-01-10 19:18:28'),(22,14,5,'BohemianRhapsody','2019-01-10 19:18:55','2019-01-10 19:18:55'),(23,15,5,'freddie','2019-01-10 19:19:02','2019-01-10 19:19:02'),(24,10,5,'Bath Duckie','2019-01-10 19:19:44','2019-01-10 19:19:44'),(25,17,9,'I like','2019-01-10 19:21:00','2019-01-10 19:21:00'),(26,20,9,'cooooollll','2019-01-10 19:21:08','2019-01-10 19:21:08'),(27,10,9,'so sweet','2019-01-10 19:21:26','2019-01-10 19:21:26'),(28,37,9,'wow','2019-01-10 19:22:22','2019-01-10 19:22:22'),(29,34,11,'funny','2019-01-11 01:14:09','2019-01-11 01:14:09'),(30,23,11,'awesome!','2019-01-11 01:16:02','2019-01-11 01:16:02'),(31,20,11,'yeah!!!!','2019-01-11 01:16:24','2019-01-11 01:16:24'),(32,47,12,'budapest','2019-01-11 01:25:08','2019-01-11 01:25:08'),(33,46,12,'I NEED TO GO HERE','2019-01-11 01:25:15','2019-01-11 01:25:15'),(34,42,12,'i want it','2019-01-11 01:25:42','2019-01-11 01:25:42'),(35,33,12,'hahaha','2019-01-11 01:26:09','2019-01-11 01:26:09'),(36,7,12,'sweet :)','2019-01-11 01:26:32','2019-01-11 01:26:32'),(37,53,13,'how cool?','2019-01-11 01:35:34','2019-01-11 01:35:34'),(38,52,13,'crystal turquoise waters','2019-01-11 01:35:52','2019-01-11 01:35:52'),(39,50,13,'beautiful','2019-01-11 01:36:08','2019-01-11 01:36:08'),(40,48,13,'new york :)','2019-01-11 01:36:27','2019-01-11 01:36:27'),(41,48,13,'<3','2019-01-11 01:36:31','2019-01-11 01:36:31'),(42,49,13,'hmmm so cute','2019-01-11 01:36:43','2019-01-11 01:36:43'),(43,44,13,'love','2019-01-11 01:36:57','2019-01-11 01:36:57'),(44,41,13,'just perfect','2019-01-11 01:37:11','2019-01-11 01:37:11'),(45,36,13,'ED SHEERAN!!!!','2019-01-11 01:37:26','2019-01-11 01:37:26'),(46,17,13,'so cute','2019-01-11 01:37:38','2019-01-11 01:37:38'),(47,20,13,'amazing','2019-01-11 01:37:48','2019-01-11 01:37:48'),(48,27,13,'yum','2019-01-11 01:38:00','2019-01-11 01:38:00'),(49,29,13,'i want it','2019-01-11 01:38:11','2019-01-11 01:38:11'),(50,8,13,'wow','2019-01-11 01:38:26','2019-01-11 01:38:26'),(51,10,13,'awwww','2019-01-11 01:38:31','2019-01-11 01:38:31'),(52,11,13,'so sweet... L)','2019-01-11 01:39:11','2019-01-11 01:39:11'),(53,49,4,'wow','2019-01-11 01:41:23','2019-01-11 01:41:23'),(54,45,4,'love it','2019-01-11 01:41:31','2019-01-11 01:41:31'),(55,39,4,'tesla','2019-01-11 01:41:49','2019-01-11 01:41:49'),(56,55,5,'fly','2019-01-11 01:42:49','2019-01-11 01:42:49'),(57,56,5,'long journey','2019-01-11 01:45:22','2019-01-11 01:45:22'),(58,46,8,'me too','2019-01-11 01:55:57','2019-01-11 01:55:57');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `countries` (
  `country_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'United States');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `followings`
--

DROP TABLE IF EXISTS `followings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `followings` (
  `user_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`following_id`),
  KEY `fk_followings_following_id_idx` (`following_id`),
  CONSTRAINT `fk_followings_following_id` FOREIGN KEY (`following_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `fk_followings_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `followings`
--

LOCK TABLES `followings` WRITE;
/*!40000 ALTER TABLE `followings` DISABLE KEYS */;
INSERT INTO `followings` VALUES (4,5,'2019-01-10 19:14:11'),(4,8,'2019-01-10 19:17:51'),(4,9,'2019-01-10 19:14:27'),(4,10,'2019-01-10 19:17:46'),(4,11,'2019-01-11 01:41:42'),(4,12,'2019-01-11 01:41:18'),(4,13,'2019-01-11 01:41:08'),(5,4,'2019-01-10 19:20:24'),(5,6,'2019-01-10 19:18:47'),(5,7,'2019-01-10 19:06:04'),(5,11,'2019-01-11 01:43:19'),(5,12,'2019-01-11 01:43:07'),(5,13,'2019-01-11 01:43:02'),(6,5,'2019-01-11 01:47:49'),(6,9,'2019-01-10 19:09:38'),(6,10,'2019-01-10 19:08:37'),(6,11,'2019-01-11 01:48:24'),(6,12,'2019-01-11 01:48:04'),(6,13,'2019-01-11 01:47:53'),(7,5,'2019-01-11 01:51:32'),(7,12,'2019-01-11 01:49:53'),(7,13,'2019-01-11 01:49:47'),(8,5,'2019-01-10 19:15:34'),(8,6,'2019-01-10 19:15:29'),(8,7,'2019-01-10 19:16:20'),(8,9,'2019-01-10 19:15:58'),(8,11,'2019-01-11 01:56:04'),(8,12,'2019-01-11 01:55:50'),(8,13,'2019-01-11 01:55:45'),(9,5,'2019-01-10 19:21:28'),(9,6,'2019-01-10 19:20:42'),(9,7,'2019-01-10 19:20:49'),(9,8,'2019-01-10 19:20:46'),(9,11,'2019-01-11 01:52:09'),(9,13,'2019-01-11 01:51:56'),(10,4,'2019-01-10 19:11:40'),(10,6,'2019-01-10 19:11:22'),(10,8,'2019-01-10 19:13:08'),(11,6,'2019-01-11 01:15:51'),(11,7,'2019-01-11 01:16:27'),(11,8,'2019-01-11 01:16:05'),(11,9,'2019-01-11 01:13:59'),(11,10,'2019-01-11 01:15:37'),(12,4,'2019-01-11 01:26:15'),(12,7,'2019-01-11 01:58:44'),(12,8,'2019-01-11 01:26:42'),(12,10,'2019-01-11 01:25:55'),(12,11,'2019-01-11 01:25:24'),(13,4,'2019-01-11 01:39:22'),(13,5,'2019-01-11 01:38:28'),(13,7,'2019-01-11 01:37:28'),(13,9,'2019-01-11 01:38:15'),(13,12,'2019-01-11 01:36:18');
/*!40000 ALTER TABLE `followings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genders`
--

DROP TABLE IF EXISTS `genders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `genders` (
  `gender_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`gender_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genders`
--

LOCK TABLES `genders` WRITE;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT INTO `genders` VALUES (2,'female'),(1,'male');
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `icons`
--

DROP TABLE IF EXISTS `icons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `icons` (
  `icon_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `icon_url` varchar(700) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`icon_id`),
  KEY `fk_icons_user_id_idx` (`user_id`),
  CONSTRAINT `fk_icons_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `icons`
--

LOCK TABLES `icons` WRITE;
/*!40000 ALTER TABLE `icons` DISABLE KEYS */;
INSERT INTO `icons` VALUES (1,4,'users/nature/icon/37130772_219216685405605_2651841279612157952_n.jpg','2019-01-10 18:05:05'),(2,5,'users/discovery/icon/21820120_132378894053561_8616386921913909248_n.jpg','2019-01-10 18:11:06'),(3,6,'users/queen/icon/36889174_210323869639938_8609241663996428288_n.jpg','2019-01-10 18:23:27'),(4,7,'users/teddysphotos/icon/15802365_1228177640596658_8518886379701141504_a.jpg','2019-01-10 18:31:24'),(5,8,'users/newyork/icon/11327015_1612119612392135_1067826995_a.jpg','2019-01-10 18:42:00'),(6,9,'users/cakes/icon/11899730_1474108546251856_1494413796_a.jpg','2019-01-10 18:50:56'),(7,10,'users/pizza/icon/11356421_1603857263217252_510625750_a.jpg','2019-01-10 18:58:26'),(8,11,'users/cars/icon/27578361_922792631213243_1394293688774950912_n.jpg','2019-01-11 01:09:20'),(9,12,'users/houses/icon/44396994_1936498276463485_67713002689789952_n.jpg','2019-01-11 01:17:38'),(10,13,'users/beachesresorts/icon/36997118_2072266299511123_8993210928510009344_n.jpg','2019-01-11 01:30:02');
/*!40000 ALTER TABLE `icons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `likes` (
  `user_id` int(20) NOT NULL,
  `post_id` int(20) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`post_id`),
  KEY `fk_likes_posts_idx` (`post_id`),
  CONSTRAINT `fk_likes_post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `fk_likes_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (4,2,'2019-01-10 19:02:39'),(4,3,'2019-01-10 19:02:43'),(4,4,'2019-01-10 19:02:36'),(4,6,'2019-01-10 19:15:13'),(4,9,'2019-01-10 19:18:01'),(4,11,'2019-01-10 19:14:09'),(4,12,'2019-01-10 19:18:07'),(4,13,'2019-01-10 19:14:19'),(4,18,'2019-01-10 19:14:05'),(4,31,'2019-01-10 19:14:25'),(4,34,'2019-01-11 01:42:10'),(4,35,'2019-01-10 19:13:53'),(4,37,'2019-01-10 19:14:44'),(4,39,'2019-01-11 01:41:45'),(4,41,'2019-01-11 01:41:56'),(4,43,'2019-01-11 01:41:52'),(4,45,'2019-01-11 01:41:32'),(4,46,'2019-01-11 01:41:38'),(4,49,'2019-01-11 01:41:24'),(4,52,'2019-01-11 01:41:10'),(4,53,'2019-01-11 01:41:12'),(4,55,'2019-01-11 01:41:05'),(5,3,'2019-01-11 01:43:58'),(5,10,'2019-01-10 19:19:39'),(5,14,'2019-01-10 19:18:50'),(5,15,'2019-01-10 19:18:58'),(5,16,'2019-01-10 19:19:12'),(5,17,'2019-01-10 19:06:08'),(5,21,'2019-01-10 19:06:12'),(5,31,'2019-01-10 19:06:42'),(5,39,'2019-01-11 01:43:20'),(5,42,'2019-01-11 01:43:25'),(5,44,'2019-01-11 01:43:17'),(5,46,'2019-01-11 01:43:09'),(5,48,'2019-01-11 01:43:12'),(5,50,'2019-01-11 01:43:00'),(5,51,'2019-01-11 01:42:52'),(5,52,'2019-01-11 01:42:55'),(5,55,'2019-01-11 01:42:45'),(5,56,'2019-01-11 01:45:15'),(6,25,'2019-01-10 19:09:59'),(6,29,'2019-01-10 19:09:34'),(6,30,'2019-01-10 19:10:46'),(6,33,'2019-01-10 19:08:34'),(6,37,'2019-01-10 19:08:15'),(6,41,'2019-01-11 01:48:22'),(6,43,'2019-01-11 01:48:15'),(6,47,'2019-01-11 01:48:06'),(6,48,'2019-01-11 01:48:09'),(6,52,'2019-01-11 01:47:54'),(6,56,'2019-01-11 01:47:47'),(6,57,'2019-01-11 01:47:44'),(7,49,'2019-01-11 01:49:55'),(7,50,'2019-01-11 01:49:45'),(7,56,'2019-01-11 01:51:30'),(7,57,'2019-01-11 01:49:41'),(7,58,'2019-01-11 01:50:56'),(8,19,'2019-01-10 19:16:24'),(8,30,'2019-01-10 19:16:12'),(8,31,'2019-01-10 19:16:01'),(8,37,'2019-01-10 19:15:39'),(8,40,'2019-01-11 01:56:05'),(8,46,'2019-01-11 01:55:52'),(8,59,'2019-01-11 01:55:39'),(8,60,'2019-01-11 01:55:36'),(9,10,'2019-01-10 19:21:27'),(9,17,'2019-01-10 19:20:54'),(9,18,'2019-01-10 19:20:51'),(9,20,'2019-01-10 19:21:10'),(9,37,'2019-01-10 19:22:18'),(9,41,'2019-01-11 01:52:11'),(9,52,'2019-01-11 01:52:01'),(9,53,'2019-01-11 01:51:55'),(9,58,'2019-01-11 01:51:51'),(9,59,'2019-01-11 01:53:04'),(10,3,'2019-01-10 19:12:38'),(10,7,'2019-01-10 19:12:14'),(10,24,'2019-01-10 19:13:18'),(10,26,'2019-01-10 19:13:06'),(10,35,'2019-01-10 19:11:38'),(10,38,'2019-01-10 19:12:48'),(11,20,'2019-01-11 01:16:16'),(11,23,'2019-01-11 01:15:56'),(11,24,'2019-01-11 01:16:08'),(11,29,'2019-01-11 01:13:56'),(11,34,'2019-01-11 01:14:04'),(11,38,'2019-01-11 01:15:49'),(11,39,'2019-01-11 01:13:42'),(11,41,'2019-01-11 01:13:50'),(11,44,'2019-01-11 01:13:47'),(12,2,'2019-01-11 01:26:18'),(12,4,'2019-01-11 01:26:20'),(12,7,'2019-01-11 01:26:33'),(12,25,'2019-01-11 01:26:46'),(12,32,'2019-01-11 01:25:58'),(12,33,'2019-01-11 01:26:03'),(12,35,'2019-01-11 01:26:24'),(12,42,'2019-01-11 01:25:32'),(12,43,'2019-01-11 01:25:26'),(12,44,'2019-01-11 01:25:22'),(12,46,'2019-01-11 01:24:54'),(12,47,'2019-01-11 01:25:00'),(12,49,'2019-01-11 01:23:03'),(13,1,'2019-01-11 01:39:20'),(13,8,'2019-01-11 01:38:21'),(13,10,'2019-01-11 01:38:32'),(13,11,'2019-01-11 01:39:14'),(13,17,'2019-01-11 01:37:39'),(13,20,'2019-01-11 01:37:44'),(13,27,'2019-01-11 01:37:55'),(13,31,'2019-01-11 01:38:17'),(13,36,'2019-01-11 01:37:19'),(13,41,'2019-01-11 01:37:04'),(13,42,'2019-01-11 01:37:14'),(13,43,'2019-01-11 01:37:01'),(13,44,'2019-01-11 01:36:53'),(13,47,'2019-01-11 01:36:48'),(13,48,'2019-01-11 01:36:16'),(13,49,'2019-01-11 01:36:36'),(13,50,'2019-01-11 01:35:55'),(13,51,'2019-01-11 01:35:21'),(13,52,'2019-01-11 01:35:44'),(13,53,'2019-01-11 01:35:23'),(13,54,'2019-01-11 01:57:06');
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `messages` (
  `message_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id_from` int(20) NOT NULL,
  `user_id_to` int(20) NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `fk_messages_users_idx` (`user_id_from`),
  KEY `fk_messages_users_id_to_idx` (`user_id_to`),
  CONSTRAINT `fk_messages_users_id_from` FOREIGN KEY (`user_id_from`) REFERENCES `users` (`user_id`),
  CONSTRAINT `fk_messages_users_id_to` FOREIGN KEY (`user_id_to`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,6,9,'hi','2019-01-10 19:10:28'),(2,10,4,'Your pic are great','2019-01-10 19:11:59'),(3,4,10,'thanks man','2019-01-10 19:14:38'),(4,8,7,'hi teddy','2019-01-10 19:16:39'),(5,8,10,'can i order?','2019-01-10 19:16:50'),(6,8,9,'You have a beautiful cakes','2019-01-10 19:17:08'),(7,5,9,'hello','2019-01-10 19:19:56'),(8,5,6,'you are awesome','2019-01-10 19:20:06'),(9,9,5,'hi there','2019-01-10 19:21:47'),(10,9,5,'how are you?','2019-01-10 19:21:53'),(11,9,8,'thank you so much','2019-01-10 19:22:04'),(12,9,6,'hello :D','2019-01-10 19:22:11'),(13,12,7,'hiiiiii','2019-01-11 01:27:00'),(14,12,4,'cool pics','2019-01-11 01:27:10'),(15,13,4,'hi there','2019-01-11 01:39:32'),(16,13,6,'how are you','2019-01-11 01:39:41'),(17,13,10,'can we order>? :).....','2019-01-11 01:40:00'),(18,4,13,'hiiiii','2019-01-11 01:42:20'),(19,4,13,'whats up','2019-01-11 01:42:28'),(20,5,9,'great','2019-01-11 01:43:37'),(21,5,9,'you>?','2019-01-11 01:43:41'),(22,6,9,'your cakes are great','2019-01-11 01:48:44'),(23,6,5,'thanks','2019-01-11 01:48:51'),(24,6,5,'you are too','2019-01-11 01:48:56'),(25,6,5,'pic are great','2019-01-11 01:49:05'),(26,6,13,'fine you?','2019-01-11 01:49:13'),(27,7,12,'hello there','2019-01-11 01:51:06'),(28,7,8,'hi :)','2019-01-11 01:51:17'),(29,9,5,'great man','2019-01-11 01:53:13'),(30,9,5,'your pics are great','2019-01-11 01:53:25'),(31,9,6,'danke','2019-01-11 01:53:58'),(32,8,7,'whats up','2019-01-11 01:56:23');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `posts` (
  `post_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `caption` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `type` enum('image','video') CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT 'image',
  `post_url` varchar(700) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `fk_posts_user_id_idx` (`user_id`),
  CONSTRAINT `fk_posts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,4,'Follow @nature: The incredible beauty of Switzerland Which photo is your favorite, 1, 2, 3 or 4? Photos by @sennarelax #nature',45.2524,19.8084,'image','users/nature/posts/;47694384_154340305554130_2364028646040264491_n.jpg;47689781_529921530750790_7313795456064400523_n.jpg;47693610_757327327968962_5705050333873583616_n.jpg;49907271_363335054461332_1735456139557481031_n.jpg','2019-01-10 18:06:37','2019-01-10 18:06:37'),(2,4,'Follow @nature: Moody Wellington Point in Queensland, Australia. Photos by @benmuldersunsets #nature',45.2523,19.8084,'image','users/nature/posts/;47694329_2269273553354582_1436575201023345865_n.jpg;47583299_285794172040228_5230929884160154426_n.jpg;47691035_354281232064303_5301437622790599617_n.jpg;47693580_594040154370701_8953005987532303733_n.jpg;49564594_367758204034364_1198358296160968154_n.jpg;49643388_775606016158015_6552263106071888008_n.jpg','2019-01-10 18:07:21','2019-01-10 18:07:21'),(3,4,'Follow @nature: Pink blossoms reflections Swipe 1, 2, 3, 5 in Osaka and 4 in Hyogo, Japan. Photos by @godive2000 #nature',45.2524,19.8084,'image','users/nature/posts/;47694640_320299975263900_9187490940902510397_n.jpg;45298966_128295574863189_7597204862958343369_n.jpg;47691027_235490800724466_6543445945690129633_n.jpg;49549840_363221180894271_1831779914841178647_n.jpg;50272919_2194247430640195_9107157912577639843_n.jpg','2019-01-10 18:08:02','2019-01-10 18:08:02'),(4,4,'Who would you take to enjoy this magical cabin with? Ukraine. Photo by @j.diegoph #nature',45.2524,19.8084,'image','users/nature/posts/;49933473_2041132985964577_8361077836379724062_n.jpg','2019-01-10 18:08:17','2019-01-10 18:08:17'),(5,4,'Comment what your expression would be with an emoji! This is where orcas belong Wild and free in the @nature Monterey, California. Photo by @slatermoorephotography #nature',45.2524,19.8084,'image','users/nature/posts/;47581776_2200312103569032_6631049701447557824_n.jpg','2019-01-10 18:08:40','2019-01-10 18:08:40'),(6,4,'Winter portrait Boston, Massachusetts, United States. Photo by @kimmyn_ #nature',45.2524,19.8084,'image','users/nature/posts/;47690845_167126604261787_1385441851209762020_n.jpg','2019-01-10 18:09:21','2019-01-10 18:09:21'),(7,4,'Follow @nature: Beautiful creatures in the wild From 1st to 10th, which one is your favorite? Photos by @soosseli #nature',45.2524,19.8084,'image','users/nature/posts/;47690275_296350204399581_6179420626044108110_n.jpg;46393203_1981923258570919_1036657888522121182_n.jpg;47581871_2007822379337390_1302356049698477841_n.jpg;47585474_478742982654391_7291676420166092529_n.jpg;47691165_357565771465530_7572064775025482690_n.jpg;47691637_589611588145329_1874665693797667002_n.jpg;47692212_129889031374242_7358043203239162680_n.jpg;47694000_566514990482514_8115150900475329631_n.jpg;47694509_213382016278671_7465385019103667630_n.jpg;49484207_991993364325902_1115309353590582042_n.jpg','2019-01-10 18:10:16','2019-01-10 18:10:16'),(8,5,'Has anyone seen Santa’s elves?\n.\n.\n.\n.\n#TigerTuesday #winter #holidays #snow #potd #nature #naturelovers #wildlifephotography #santa #elves #christmas #ProjectCAT #bigcats #tigers #happyholidays',45.2524,19.8084,'image','users/discovery/posts/;46580058_220028148903346_1066882835830215691_n.jpg','2019-01-10 18:11:27','2019-01-10 18:11:27'),(9,5,'“This is a Leucistic Ball Python. People often confuse albino animals with leucistic ones. Albino animals lack melanin. Which makes their skin and hair white, or yellowish. They usually have red or pink eyes. Leucistic animals have recessive pigmentation genes, and often have blue eyes.” by Mark Kostich (@mark_kostich_photography)\n.\n.\n.\n.\n#potd #snake #python #LeucisticBallPython #wow #instanature #reptile #reptilesofinstagram #holidays #snow',45.2524,19.8084,'image','users/discovery/posts/;46888235_117770172595328_3411855452883071813_n.jpg','2019-01-10 18:11:42','2019-01-10 18:11:42'),(10,5,'“Bath Duckie ”  + caption by Roeselien Raimond (@roeselienraimond)\n.\n.\n.\n.\n#2019 #potd #cute #duckling #baby #nature #naturelovers #duck #babyduck #nature_brilliance #bathtime',45.2524,19.8084,'image','users/discovery/posts/;47692694_355597721662578_2115013946311936237_n.jpg','2019-01-10 18:12:24','2019-01-10 18:12:24'),(11,5,'\"The Opera Singer” + caption by Ankit Bansod (@ankit.bansod13)\n.\n.\n.\n.\n#TigerTuesday #instanature #nature #naturephotography #wildlifephotography #ProjectCAT #bigcats #tigers',45.2524,19.8084,'image','users/discovery/posts/;49759359_2392335320841684_9106952837786271218_n.jpg','2019-01-10 18:12:43','2019-01-10 18:12:43'),(12,5,'“This beautiful Napoleon Wrasse can cost up to $25,000 on the sushi trade black market, where the animal is eaten while still alive. The island of Palau continues to strengthen its tourism policies to help conserve its biodiverse reef system and protect its natural resources from illegal fishing as well as poaching. We are all a voice for the conservation of our planet, use yours.” + caption by Gregory Piper (@gregpiperarts)\n.\n.\n.\n.\n#potd #colorful #Palau #conserve #ocean #underwater #wild #NapoleonWrasse #giant #fish',45.2524,19.8084,'image','users/discovery/posts/;45551633_561836257611268_3654783301695079059_n.jpg','2019-01-10 18:14:53','2019-01-10 18:14:53'),(13,5,'“Fly me to the moon.\"\n.\n.\n\n.\n.\n + caption by Donal Boyd (@DonalBoyd)\n.\n.\n.\n.\n#potd #moon #weekend #Iceland #sky #night #travel #adventure #fullmoon',45.2524,19.8084,'image','users/discovery/posts/;43778810_294065764543382_2341285663216113447_n.jpg','2019-01-10 18:16:56','2019-01-10 18:16:56'),(14,6,'Thank YOU for making #BohemianRhapsody the top trending song of 2018.\n\n@google #yearinsearch',45.2524,19.8084,'image','users/queen/posts/;46103258_309336629681422_3939930993061725750_n.jpg','2019-01-10 18:23:40','2019-01-10 18:23:40'),(15,6,'5 September 1946 – 24 November 1991 ❤\n•\n#queen #freddiemercury #queenband',45.2524,19.8084,'image','users/queen/posts/;46144482_707957166256134_3105830860585471691_n(1).jpg','2019-01-10 18:24:43','2019-01-10 18:24:43'),(16,6,'FAN FEATURE: Queen and the King of Pop: The Parallel Evolution and Lasting Friendship of Michael Jackson and Queen by Sam Geden. Read on Queen Online.\n•\n#queen #queenband #fanfeature #freddiemercury #michaeljackson',45.2524,19.8084,'image','users/queen/posts/;43278069_327002188077061_8283564477038434833_n.jpg','2019-01-10 18:25:49','2019-01-10 18:25:49'),(17,7,'Who likes sport sports',45.2524,19.8084,'image','users/teddysphotos/posts/;47581276_2426691170751768_9100212934813941760_n.jpg','2019-01-10 18:32:15','2019-01-10 18:32:15'),(18,7,'Chappelle party in Joburg with @fredyonnet @candytman',45.2524,19.8084,'image','users/teddysphotos/posts/;45404686_1930147020366274_1575790086056837120_n.jpg','2019-01-10 18:34:21','2019-01-10 18:34:21'),(19,7,'You’re a wizard, Frodo',45.2524,19.8084,'image','users/teddysphotos/posts/;44564135_348520539292150_6002092139029200896_n.jpg','2019-01-10 18:35:22','2019-01-10 18:35:22'),(20,7,'Atlanta!  @zakarywalters',45.2524,19.8084,'image','users/teddysphotos/posts/;44344080_478714952621142_5899716430978351104_n.jpg;43020824_370847490319275_1103710134401499136_n.jpg;43436855_2229702167355929_3438936784413655040_n.jpg;43464242_202647907302648_5739693779254771712_n.jpg;43486892_719104525131869_5299080428752207872_n.jpg;43772856_202411203994018_8387582726881935360_n.jpg;43985884_337438320139185_7906886284851806208_n.jpg;44503414_742654892748032_2023420038731530240_n.jpg','2019-01-10 18:36:54','2019-01-10 18:36:54'),(21,7,'Some shots from tonight’s Detroit show from @zakarywalters',45.2524,19.8084,'image','users/teddysphotos/posts/;40369488_256071798378498_6653478252213960704_n.jpg;39803327_472971679852478_8407344859253309440_n.jpg;39999641_321216341777533_3903642968711495680_n.jpg;40129227_307841660019538_3794780421229641728_n.jpg;40332791_289384125207198_1939077837499662336_n.jpg;40395077_461463844348481_3065397796438802432_n.jpg;40479858_2067347770181433_5366526924426313728_n.jpg;40598459_465664713926146_9214633977430671360_n.jpg;40927889_268476080461424_6741460687337291776_n.jpg','2019-01-10 18:39:06','2019-01-10 18:39:06'),(22,8,'Sidewalks of New York',45.2524,19.8084,'image','users/newyork/posts/;49462080_214553486159333_2465190097993854240_n.jpg','2019-01-10 18:42:50','2019-01-10 18:42:50'),(23,8,'New York: So much love for all the savy small businesses leveraging social and online commerce to innovate and sell while at the same time giving back to the community through philanthropy, sustainability and local initiatives. Now that’s what it really means to be a small business with a big heart! Thanks @Dell for the invite to the Fast Company Innovation Festival. #DellSmallBiz #dellxps #FCFestival #ad',45.2523,19.8084,'image','users/newyork/posts/;43984844_184498169096486_3641291988151136540_n.jpg;42872846_120746918911836_8324560462572147479_n.jpg;43378329_297444701095394_4042460439171827663_n.jpg;43817402_122906568693027_291160293708216343_n.jpg','2019-01-10 18:44:02','2019-01-10 18:44:02'),(24,8,NULL,45.2524,19.8084,'image','users/newyork/posts/;36147625_1762895407162819_534611505799233536_n.jpg;35574908_224666688361022_2193511592740519936_n.jpg;35617175_258703641379652_647790435284025344_n.jpg;36033915_2090641921262564_377929428098875392_n.jpg;36113642_1024774947688844_6177027776197951488_n.jpg;36147707_207526889899316_5170013874073632768_n.jpg;36643823_1768222166624413_353186078556946432_n.jpg','2019-01-10 18:45:16','2019-01-10 18:45:16'),(25,8,'A Mother Mourns Forever. 9/11',45.2524,19.8084,'image','users/newyork/posts/;40536262_283719668897112_2482456276149786622_n.jpg','2019-01-10 18:46:07','2019-01-10 18:46:07'),(26,8,'New York entrepreneurs gathered in Brooklyn at the #DellSmallBiz pop-up event to celebrate #smallbusinessweek with @Dell. It was so inspiring. They shared their stories in a set of talks, educational sessions and tech demos. Here’s a sneak peek. Full sessions are available at Dell.com/SBweek #ad',45.2523,19.8084,'image','users/newyork/posts/;31047950_161581224681659_3685603486436687872_n.jpg;31047944_214453742481180_7749443589918162944_n.jpg;31108385_174464313215088_5962918042970095616_n.jpg;31184303_1894582557506588_5927720386547417088_n.jpg','2019-01-10 18:48:02','2019-01-10 18:48:02'),(27,9,'Which one is your favorite? I\'m in love with them all! : @thepurplecupcake_',45.2524,19.8084,'image','users/cakes/posts/;47695028_598361643936510_8430302735473829063_n.jpg;47582095_507759479731192_5500079452693802618_n.jpg;47582237_679921355738178_7540464901629608546_n.jpg;47691875_788988404814129_4243416906465896433_n.jpg;47693100_218843495722176_6184174012037698299_n.jpg','2019-01-10 18:52:01','2019-01-10 18:52:01'),(28,9,'Frosted Blue!',45.2524,19.8084,'image','users/cakes/posts/;47585918_229293901297941_845592251380032265_n.jpg','2019-01-10 18:52:44','2019-01-10 18:52:44'),(29,9,'Incredible cake!',45.2524,19.8084,'image','users/cakes/posts/;46191446_2453054238057536_2392553320722377658_n.jpg;46529582_2214801518735702_1258082634158520475_n.jpg;47061598_209507946600186_6677194363679630175_n.jpg;47248497_528088167601155_1870489424904586968_n.jpg','2019-01-10 18:53:30','2019-01-10 18:53:30'),(30,9,'All Are So Beautiful! Which is your favorite??? : @ateliemarinamoucachen',45.2524,19.8084,'image','users/cakes/posts/;45855517_285501108748015_478618124300090606_n.jpg;44227285_122295788770098_3923140742252489344_n.jpg;44559296_104188730547983_3858953351729738392_n.jpg','2019-01-10 18:54:25','2019-01-10 18:54:25'),(31,9,'1-10 which is your favorite?Incredible!  @gulnarafedorova',45.2524,19.8084,'image','users/cakes/posts/;42775771_2205213176422567_9045824134344501207_n.jpg;43419269_286587898659422_4998055376171847692_n.jpg;43495897_265968207395652_4023907154756017195_n.jpg;43693426_549937992145747_2909247858609491956_n.jpg;43914213_350726085675941_8998566657827323015_n.jpg;43985136_764849607197130_1279779342446386076_n.jpg;44227285_122295788770098_3923140742252489344_n.jpg;44249682_324870054995430_1656533319072031533_n.jpg;46191446_2453054238057536_2392553320722377658_n.jpg;47582237_679921355738178_7540464901629608546_n.jpg','2019-01-10 18:56:49','2019-01-10 18:56:49'),(32,10,'Rare photo of a pizza kiss getting God’s blessing. #EEEEEATS #DailyPizza : @outsidepizza',45.2523,19.8084,'image','users/pizza/posts/;47690620_272646680069811_5522230330296433140_n.jpg','2019-01-10 18:59:06','2019-01-10 18:59:06'),(33,10,'Mommy’s busy sweetie. #EEEEEATS #DailyPizza : @jenn_tes',45.2524,19.8084,'image','users/pizza/posts/;47586535_620428171748263_762479351233491160_n.jpg','2019-01-10 18:59:58','2019-01-10 18:59:58'),(34,10,'American hero. #EEEEEATS #DailyPizza',45.2524,19.8084,'image','users/pizza/posts/;47404044_361403881338071_6400741129883688029_n.jpg','2019-01-10 19:00:55','2019-01-10 19:00:55'),(35,4,'When architecture meets @nature Bosco Verticale building in Milan, Italy. Photo by @ssnnas #nature',45.2524,19.8084,'image','users/nature/posts/;47581526_220386782241775_6067136457041363831_n.jpg','2019-01-10 19:02:26','2019-01-10 19:02:26'),(36,7,'Scary Harry  @zakarywalters',45.2524,19.8084,'image','users/teddysphotos/posts/;42561259_329467404300783_3645055786821877760_n.jpg','2019-01-10 19:03:48','2019-01-10 19:03:48'),(37,5,'“Now that all six elephants have been successfully relocated, they will finally have the opportunity to interact with wild herds in Zimbabwe. As rescued animals, they will live alongside their caretakers for support and protection until they’re ready to be fully independent. .\n.\n.\nOver the coming months I’ll continue to share more about ZEN and these six elephants. Follow their transition into a new life in the wild with me -- @donalboyd.  Happy World Elephant Day everyone!”  + caption by Donal Boyd (@donalboyd)\n.\n.\n.\n.\n#Elephants #elephantlove #worldelephantday #wed #donalboyd #ZimbabweElephantNursery #ZEN #elephantday #potd #weekend',45.2524,19.8084,'image','users/discovery/posts/;37859142_397590550768419_8566449869739261952_n.jpg','2019-01-10 19:05:40','2019-01-10 19:05:40'),(38,6,'\"We\'re family. We believe in each other. That\'s everything.\" - Freddie Mercury\n•\n @paolakudacki\n•\n@mercury_motg #queen #bohemianrhapsody #ramimalek #goldenglobes #goldenglobes2019 #brianmay #rogertaylor',45.2524,19.8084,'image','users/queen/posts/;47690106_346360379428476_3234434233507879683_n.jpg;47586091_2187929994801730_599867471349564772_n.jpg;49763267_300028567311162_367963130287832491_n.jpg','2019-01-10 19:08:10','2019-01-10 19:08:10'),(39,11,'Fully electric Tesla Roadster! Thoughts?\nBy: @bsl_production\n#cars #tesla @teslamotors',45.2524,19.8084,'image','users/cars/posts/;44341445_248763805797606_2148149425156073282_n.jpg;44387591_135445400770271_3370288717112174988_n.jpg','2019-01-11 01:10:19','2019-01-11 01:10:19'),(40,11,'Two-faced Charger! Thoughts?\nBy: @1sik392\n#cars #charger',45.2524,19.8084,'image','users/cars/posts/;43229706_513278469190961_1463503140761124535_n.jpg','2019-01-11 01:11:05','2019-01-11 01:11:05'),(41,11,'Generations of the Ford GT/GT40!\nWhich one is your favorite?\nBy: @drewphillipsphoto\n#cars #ford',45.2524,19.8084,'image','users/cars/posts/;41812827_538377349935376_2774036518887872625_n.jpg','2019-01-11 01:11:39','2019-01-11 01:11:39'),(42,11,'Audi RS5 coupe! Thoughts?\nBy: @mateo.r.photography\n#cars',45.2524,19.8083,'image','users/cars/posts/;41737028_406541006545859_8476228857054070997_n.jpg','2019-01-11 01:12:19','2019-01-11 01:12:19'),(43,11,'Shelby GT350! Thoughts?\nBy: @aclean350\n#cars #ford',45.2524,19.8084,'image','users/cars/posts/;41687562_1925602130838321_1755902372968214663_n.jpg','2019-01-11 01:12:47','2019-01-11 01:12:47'),(44,11,'Widebody hellcat! Thoughts?\nBy: @torred707\n#cars #hellcat',45.2524,19.8084,'image','users/cars/posts/;47694539_558293454637768_2840649086933944996_n.jpg','2019-01-11 01:13:25','2019-01-11 01:13:25'),(45,12,'The Glenhaven Residence\nDesigned by @abramsonteigerarchitects\nPhotos by @jimbartschphotographer\n-\n\n#houses #architecture #realtor #luxuryhomes #photography #realestatephotographer #contemporaryhome #homestaging #interiordesign #builder #customhomes',45.2524,19.8084,'image','users/houses/posts/;47586342_1067148953491863_1635302919530509125_n.jpg;46615011_1111227182396942_1136244218614560490_n.jpg;47584796_136367757373468_1886858434851838837_n.jpg;47586091_1323333951139974_2480206544646367329_n.jpg;47586137_1995603200734194_749888900569760512_n.jpg;47692022_906834613146703_2177895691420519006_n.jpg;47692105_128940571471780_6130596128434160387_n.jpg;49473728_220701968870777_3481805377834754907_n.jpg','2019-01-11 01:19:24','2019-01-11 01:19:24'),(46,12,'Tag someone you’d take here \nThe Grotto Spa at Tigh-Na-Mara  Enjoy a soothing dip in the mineral pool, relax in the peaceful warmth of a Spa treatment, and rest fireside with tea and endless tapas dining. I NEED TO GO HERE ASAP!! \nCredit: @grotto.spa -\n\n#architecture #realtor #spa #resort #relax #realestate #luxuryhomes #luxuryliving #photography #interiordesign #grotto #realestateagent #luxuryrealestate',45.2524,19.8084,'image','users/houses/posts/;49907407_2323636540994549_5920807612438876906_n.jpg','2019-01-11 01:20:39','2019-01-11 01:20:39'),(47,12,'Tag someone you’d do this with \nPhoto by @njwhite | @houses  .\n\n#budapest #hungary #fireworks #realestate #travel #luxury #realestate #customhomes #builder #interiors #view',45.2524,19.8084,'image','users/houses/posts/;47581444_739092459805773_2781393154994418166_n.jpg','2019-01-11 01:21:15','2019-01-11 01:21:15'),(48,12,'Manhattan Winter & Summer \nPhoto by @beholdingeye\nHappy New Years!! #2019 -\n\n#newyork #architecture #realtor #luxuryhomes #nyc #aerial #drone #newyorkcity #realestate #love #newyear #newyearseve #beautiful',45.2524,19.8084,'image','users/houses/posts/;47583592_2025772310825542_3234266329416957917_n.jpg','2019-01-11 01:22:03','2019-01-11 01:22:03'),(49,12,'Tree house \nPhotos by @kylefinndempsey\nLocated in Maine, USA  -\n#houses #nature #architecture #realtor #broker #developer #customhomes #builder #designer #interiordesign #photography #realestate #luxuryhomes',45.2524,19.8084,'image','users/houses/posts/;47582082_1971684869535504_8852812382650630785_n.jpg;46843556_594833727621629_2595077724046427510_n.jpg;47694033_871288189928394_7114083082563863802_n.jpg','2019-01-11 01:22:58','2019-01-11 01:22:58'),(50,13,'Watch the sun set with the whole gang on this cozy deck. We\'ll save you a seat. \nBeaches Turks and Caicos\n#BeachesResorts #TurksandCaicos #GroupTravel',45.2524,19.8084,'image','users/beachesresorts/posts/;47693348_1115795295248367_2759119527970854846_n.jpg','2019-01-11 01:30:41','2019-01-11 01:30:41'),(51,13,'It\'s Saturday evening in #paradise. The breeze is warm, the tropical vibes are contagious and the ones you love most are all here. \nBeaches Turks and Caicos\n#BeachesResorts #AllInclusive',45.2524,19.8084,'image','users/beachesresorts/posts/;43913100_2058740501084437_4653205286549076211_n.jpg','2019-01-11 01:32:03','2019-01-11 01:32:03'),(52,13,'Take a stroll down the powder-white sand of Negril’s famous 7-mile beach, while crystal turquoise waters lap the shore. Who are you bringing to #paradise?Beaches Negril\n#BeachesResorts #AllInclusive #Beach #Negril',45.2524,19.8084,'image','users/beachesresorts/posts/;44476730_349624055791898_246351227376471068_n.jpg','2019-01-11 01:32:29','2019-01-11 01:32:29'),(53,13,'The night comes to life with our on-resort entertainment! From #beachparty to teen club, we’re heating things up. \nBeaches Negril\n#BeachesResorts #FamilyVacation',45.2524,19.8084,'image','users/beachesresorts/posts/;44369639_262255484487768_449079311487176245_n.jpg','2019-01-11 01:32:58','2019-01-11 01:32:58'),(54,13,'Welcome to paradise -xo Beaches Ocho Rios',45.2524,19.8084,'image','users/beachesresorts/posts/;31905484_1082522491913286_9195756732876324864_n.jpg','2019-01-11 01:34:41','2019-01-11 01:34:41'),(55,13,'It\'s official, \'We\'re on vacation\'  @listen2lena',45.2524,19.8084,'image','users/beachesresorts/posts/;32063888_1699408616833369_3013772882246369280_n.jpg','2019-01-11 01:35:17','2019-01-11 01:35:17'),(56,5,'These young white lions have a long journey ahead of them. Currently, there is no official international law set in place to protect white lions from becoming extinct, why do you think that is?\n.\n.\n.\n.\n#potd #bigcats #protectwhitelions #projectcat #cubs #wild #explore #protect #nature #whitelion',45.2524,19.8084,'image','users/discovery/posts/;47043581_1963939450578081_2309394096157108589_n.jpg','2019-01-11 01:45:06','2019-01-11 01:45:06'),(57,6,'We’re excited to announce Queen + @adamlambert will be debuting the brand new Rhapsody touring show across 23 North America dates July-August 2019. Tickets go on general public sale December 7th over at LiveNation.com, 10 AM local time. Queen + Adam Lambert Fan Club Presales will run from 10AM-10PM local time on Thursday, December 6th.\n\nThe Rhapsody Tour - North America 2019\nWed Jul 10 Vancouver, BC - Rogers Arena\nFri Jul 12 - Tacoma, WA - Tacoma Dome\nSun Jul 14 - San Jose, CA - SAP Center\nTue Jul 16 - Phoenix, AZ - Talking Stick Resort Arena\nFri Jul 19 - Los Angeles, CA - The Forum\nTue Jul 23 - Dallas, TX - American Airlines Center\nWed Jul 24 - Houston, TX - Toyota Center\nSat Jul 27 - Detroit, MI - Little Caesars Arena\nSun Jul 28 - Toronto, ON - Scotiabank Arena\nTue Jul 30 - Washington, DC - Capital One Arena\nWed Jul 31 - Pittsburgh, PA - PPG Paints Arena\nSat Aug 03 - Philadelphia, PA - Wells Fargo Center\nSun Aug 04 - Boston, MA - Xfinity Center\nTue Aug 06 - New York, NY - Madison Square Garden\nFri Aug 09 - Chicago, IL - United Center\nSat Aug 10 - St. Paul, MN - Xcel Energy Center\nTue Aug 13 - Columbus, OH - Nationwide Arena\nThu Aug 15 - Nashville, TN - Bridgestone Arena\nSat Aug 17 - Ft. Lauderdale, FL - BB&T Center\nSun Aug 18 - Tampa, FL - Amalie Arena\nTue Aug 20 - New Orleans, LA - Smoothie King Center\nThu Aug 22 - Atlanta, GA - State Farm Arena\nFri Aug 23 - Charlotte, NC - Spectrum Center\n\nMore at QueenOnline.com ⠀⠀⠀⠀⠀⠀⠀⠀⠀\nPhotographer: Bojan Hohnjec\n© Miracle Productions',45.2524,19.8084,'image','users/queen/posts/;46285910_2094356884210698_7197356546262298443_n.jpg','2019-01-11 01:47:28','2019-01-11 01:47:28'),(58,7,'Splash  @zakarywalters',45.2524,19.8084,'image','users/teddysphotos/posts/;46546972_361013747996358_1195868750910324736_n.jpg','2019-01-11 01:50:52','2019-01-11 01:50:52'),(59,9,'Incredible!',45.2524,19.8084,'image','users/cakes/posts/;44622173_352888368621031_296764009877775080_n.jpg;44482887_326276914855945_262465574555233825_n.jpg;44547777_354463775321952_7420175306551415233_n.jpg','2019-01-11 01:53:02','2019-01-11 01:53:02'),(60,8,'New York Streets.',45.2524,19.8084,'image','users/newyork/posts/;47694738_360308788082653_2272473037397527399_n(1).jpg','2019-01-11 01:55:33','2019-01-11 01:55:33');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `states` (
  `state_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `country_id` int(20) NOT NULL,
  PRIMARY KEY (`state_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_states_country_id` (`country_id`),
  CONSTRAINT `fk_states_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='													';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (1,'New York',1),(2,'California',1),(3,'Alabama',1),(4,'Arizona',1),(5,'Alaska',1),(6,'Arkanzas',1),(7,'Colorado',1),(8,'Connecticut',1),(9,'Delaware',1),(10,'Florida',1),(11,'Georgia',1),(12,'Hawaii',1),(13,'Idaho',1),(14,'Illionis',1),(15,'Indiana',1),(16,'Iowa',1),(17,'Kansas',1),(18,'Kentucky',1),(19,'Louisiana',1),(20,'Maine',1),(21,'Maryland',1),(22,'Massachusetts',1),(23,'Michigan',1),(24,'Minnesota',1),(25,'Mississippi',1),(26,'Missouri',1),(27,'Montana',1),(28,'Nebraska',1),(29,'Nevada',1),(30,'New Hampshire',1),(31,'New Jersey',1),(32,'New Mexico',1),(33,'North Carolina',1),(34,'North Dakota',1),(35,'Ohio',1),(36,'Oklahoma',1),(37,'Oregon',1),(38,'Pennsylvania',1),(39,'Rhode Island',1),(40,'South Carolina',1),(41,'South Dakota',1),(42,'Tennessee',1),(43,'Texas',1),(44,'Utah',1),(45,'Vermont',1),(46,'Virginia',1),(47,'Washington',1),(48,'West Virginia',1),(49,'Wisconsin',1),(50,'Wyoming',1);
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `real_name` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `gender_id` int(20) DEFAULT NULL,
  `city_id` int(20) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_gender_id_idx` (`gender_id`),
  KEY `fk_users_city_id_idx` (`city_id`),
  CONSTRAINT `fk_users_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`),
  CONSTRAINT `fk_users_gender_id` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`gender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (4,'nature@gmail.com','nature','123','Nature',1,18,'1988-06-14 00:00:00','2019-01-10 18:04:56',NULL,1),(5,'discovery@gmail.com','discovery','123','Discovery',2,20,'1978-08-11 00:00:00','2019-01-10 18:10:58',NULL,1),(6,'queen@gmail.com','queen','123','Queen',1,7,'1978-05-04 00:00:00','2019-01-10 18:23:19','2019-01-11 00:47:39',1),(7,'ed.sheeran@gmail.com','teddysphotos','123','Ed Sheeran',1,10,'1991-02-16 00:00:00','2019-01-10 18:31:05',NULL,1),(8,'newyork@gmail.com','newyork','123','New York',1,1,'2006-10-04 00:00:00','2019-01-10 18:41:54',NULL,1),(9,'cakes@gmail.com','cakes','123','Cakes',2,26,'1998-12-07 00:00:00','2019-01-10 18:50:42',NULL,1),(10,'pizza@gmail.com','pizza','123','Pizza',1,68,'1995-01-04 00:00:00','2019-01-10 18:58:19',NULL,1),(11,'cars@gmail.com','cars','123','Cars',2,81,'2000-04-11 00:00:00','2019-01-11 01:09:06',NULL,1),(12,'houses@gmail.com','houses','123','Houses',1,49,'1994-06-23 00:00:00','2019-01-11 01:17:23',NULL,1),(13,'beaches.resorts@gmail.com','beachesresorts','123','Beaches Resorts',2,82,'2004-05-29 00:00:00','2019-01-11 01:29:51','2019-01-11 01:58:16',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-11  1:59:47
